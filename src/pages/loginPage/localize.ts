const LOCALIZE_LOGINPAGE = {
    'de': {
        login: "anmeldung",
        signIn: "registrieren ",
        username_or_password_incorrect: "Benutzername oder Passwort falsch !",
        enter_valid_email: "Bitte geben Sie eine gültige E-Mail-Adresse ein!",
        passwords_not_same: "Passwörter sind nicht gleich !",
        checkout_email: "Überprüfen Sie jetzt Ihre E-Mail-Adresse, um Ihr Konto zu bestätigen!",
        email_already_exist: "E-Mail Adresse existiert bereits ! Bitte überprüfen Sie !",
        inputEmail: "Deine E-Mail *",
        inputPwd: "Ihr Passwort *",
        submit: "Einreichen",
        forget_Password: "Passwort vergessen?",

    },
    'fr': {
        login: "connexion",
        signIn: "s'inscrire",
        username_or_password_incorrect: "Nom d'utilisateur ou mot de passe incorrect !",
        enter_valid_email: "Veuillez entrer un email valide !",
        passwords_not_same: "Les mots de passe ne sont pas les mêmes !",
        checkout_email: "Vérifiez maintenant votre adresse email pour valider votre compte !",
        email_already_exist: "L'adresse e-mail existe déjà ! S'il vous plaît vérifier !",
        inputEmail: "Votre Email *",
        inputPwd: "Votre mot de passe *",
        submit: "Soumettre",
        forget_Password: "Mot de passe oublié ?",

    },

    'en': {
        login: "login",
        signIn: "register",
        username_or_password_incorrect: "Username or password incorrect !",
        enter_valid_email: "Please enter valid email !",
        passwords_not_same: "Passwords are not the same !",
        checkout_email: "Check now your email adresse to validate your account !",
        email_already_exist: "Email adresse already exist ! Please check out !",
        inputEmail: "Your Email *",
        inputPwd: "Your Password *",
        submit: "Submit",
        forget_Password: "Forget Password?",

    },
};

export default LOCALIZE_LOGINPAGE;