import * as React from 'react';


// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE


// IMPORT STYLES ZONE
import './loginPage.scss';
// END IMPORT STYLES ZONE

// IMPORT STYLES ZONE
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE_LOGINPAGE from './localize'
// END IMPORT LOCALIZE ZONE

// IMPORT API ZONE
// END IMPORT API ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../assets/uiComponents/base/page/pageBase';
import LoginForm from './components/loginForm/loginForm';
import SignInForm from './components/signInForm/signInForm';
import ForgetPwdPopup from './components/popups/forgetPwd/forgetPwd';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import AppIcon from '../../assets/img/icon.svg';
// END IMPORT IMAGE ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TPages,
    TLanguages
} from '../../commonInterface';
// END IMPORT INTERFACE ZONE

interface IProps {
    goToPage: (pageName: TPages) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
    showSplashscreenPage: (show: boolean) => void
}

interface IState {
    formToShow: string,
    translator: any;
}

export default class LoginPage extends PageBase<IProps, IState> {

    protected componentId = 'loginPage';

    constructor(props: IProps) {
        super(props);

        this.state = {
            formToShow: 'logIn',
            translator: LOCALIZE_LOGINPAGE,
        };
    }

    protected init = (): void => {
        this.props.showSplashscreenPage(false);
    }



    protected clear(): void { }

    protected goToForm(formToGo: string) {
        this.setState({
            formToShow: formToGo,
        });
    }

    protected formToShowRender() {
        if (this.state.formToShow === 'signIn') {
            return <SignInForm
                translator={this.state.translator[this.props.language]}
                showSimpleModal={this.props.showSimpleModal}
            />;
        }

        return <LoginForm
            translator={this.state.translator[this.props.language]}
            goToPage={this.props.goToPage}
            showSimpleModal={this.props.showSimpleModal}
            openPopup={this.openPopup}
        />;
    }

    protected popupsRender(): React.ReactNode {
        return (
            <div className="popups-ctn">
                <ForgetPwdPopup
                    goToPage={() => { }}
                    language={this.props.language}
                    showSimpleModal={this.props.showSimpleModal}
                />
            </div>
        )
    }

    render(): React.ReactNode {
        return (
            <div id={this.setComponentId()} className="page page-transition with-scrollbar">

                <div id={this.setIdTo("iconCtn")}>
                    <AppIcon />
                </div>

                <div id={this.setIdTo('formCtn')}>
                    <div id={this.setIdTo('form')}>

                        <div id={this.setIdTo("buttonCtn")} className="margin-side-xxl">
                            <button
                                type="button"
                                onClick={() => this.goToForm('logIn')}
                                className={`btn btn-lg ${this.state.formToShow === 'logIn' ? 'btn-primary' : ''}`}>
                                {this.state.translator[this.props.language].login}
                            </button>
                            <button
                                type="button"
                                onClick={() => this.goToForm('signIn')}
                                className={`btn btn-lg ${this.state.formToShow === 'signIn' ? 'btn-primary' : ''}`}>{this.state.translator[this.props.language].signIn}
                            </button>
                        </div>
                        {this.formToShowRender()}
                    </div>
                </div>

                <div id={this.setIdTo("appPresentation")}>
                    <h2>Presentation (TODO):</h2>
                    {/* TODO: Write start page to help user to create account, pay, etc ... */}
                </div>

                {this.popupsRender()}
            </div>
        );
    }
}
