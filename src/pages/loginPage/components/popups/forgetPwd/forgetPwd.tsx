import * as React from 'react';
import IconHandler from '../../../../../assets/uiComponents/iconHandler/iconHandler';


// IMPORT HELPER ZONE
import Helper from '../../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './forgetPwd.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PopupBase from '../../../../../assets/uiComponents/base/popup/popupBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TPages,
    TLanguages,
    IForgetPwdData,
    ISendForgetPwdMailResponse,
} from "../../../../../commonInterface";
// END IMPORT INTERFACE ZONE

interface IProps {
    goToPage: (pageName: TPages) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
}

interface IState {
    textInEmailInput: string,
    translator: any;
}


export default class ForgetPwdPopup extends PopupBase<IProps, IState> {

    protected componentId = 'forgetPwd';
    protected popupName = 'Forget password';

    constructor(props: IProps) {
        super(props);

        this.state = {
            textInEmailInput: '',
            translator: LOCALIZE[this.props.language],
        };
    }

    protected init(): void { }

    protected clear(): void {
        this.setState((prevState: IState) => ({
            textInEmailInput: '',
        }))
    }

    protected onEmailInputChange = (event): void => {
        this.setState({
            textInEmailInput: event.target.value,
        });
    }

    protected onKeyPress = (event): void => {
        if (event.key === 'Enter') {
            this.onSubmitBtnClick();
        }
    }

    protected areInputsValid = (): boolean => {
        if (!Helper.checkEmail(this.state.textInEmailInput)) {
            this.props.showSimpleModal({
                type: 'danger',
                message: LOCALIZE[this.props.language]['enter_valid_email'],
            });

            return false;
        }
        return true;
    }

    protected getForgetPwdData(): IForgetPwdData {
        return {
            email: this.state.textInEmailInput
        }
    }

    protected onSubmitBtnClick() {
        if (this.areInputsValid()) {
            ApiCmds.sendForgetPwdMail(this.getForgetPwdData(),
                (result: ISendForgetPwdMailResponse) => {
                    this.onSubmitBtnClickCallback(result);
                })
        }
    }

    protected onSubmitBtnClickCallback = (result: ISendForgetPwdMailResponse): void => {
        if (result.success) {
            if (result.response.hasEmailBeSend) {
                this.props.showSimpleModal({
                    type: 'success',
                    message: LOCALIZE[this.props.language]['forget_pwd_mail_send'],
                })
            }

        }
    }

    protected popupBodyRender(): React.ReactNode {
        const localize = LOCALIZE[this.props.language];

        return (
            <div className="popup-body card card-body">
                <h4 className="card-title">{localize['title']}</h4>
                {/* TODO: Write better text (in LOCALIZE) */}
                <p className="card-text">Boot polish is a waxy paste, cream, or liquid used to polish, shine, and waterproof leather shoes or boots to extend the footwear's life, and restore, maintain and improve their appearance.</p>
                <input
                    type="text"
                    className="form-control"
                    value={this.state.textInEmailInput}
                    onChange={this.onEmailInputChange}
                    onKeyPress={this.onKeyPress}
                    placeholder={localize['inputEmail']}
                />

                <button type="button" className="btn btn-primary" onClick={() => this.onSubmitBtnClick()}>
                    <IconHandler icon="paper-plane" />
                    {localize['submit']}
                </button>
            </div>
        )
    }
}
