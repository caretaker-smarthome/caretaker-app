const LOCALIZE = {
    'de': {
        title: "Enter your email and check your account (don't forget to check in spam)",
        inputEmail: "Deine E-Mail *",
        enter_valid_email: "Bitte geben Sie eine gültige E-Mail-Adresse ein!",
        submit: "Einreichen",
        forget_pwd_mail_send: "Die E-Mail zur Passwortwiederherstellung wurde Ihnen zugesandt !",
    },
    'fr': {
        title: "Enter your email and check your account (don't forget to check in spam)",
        inputEmail: "Votre Email *",
        enter_valid_email: "Veuillez entrer un email valide !",
        submit: "Soumettre",
        forget_pwd_mail_send: "L'email de récupération de mot de passe vous a été envoyé !",
    },

    'en': {
        title: "Enter your email and check your account (don't forget to check in spam)",
        inputEmail: "Your Email *",
        enter_valid_email: "Please enter valid email !",
        submit: "Submit",
        forget_pwd_mail_send: "The password recovery email has been sent to you !",
    },
};

export default LOCALIZE;