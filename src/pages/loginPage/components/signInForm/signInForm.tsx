import * as React from 'react';
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';



// IMPORT APICMDS ZONE
import ApiCmds from '../../../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

// INIT HELPER METHODS ZONE
import Helper from '../../../../helper';
// END INIT HELPER METHODS ZONE

// IMPORT STYLES ZONE
import './signInForm.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import FrFlagSvg from '../../../../assets/img/fr.svg';
// @ts-ignore
import DeFlagSvg from '../../../../assets/img/de.svg';
// @ts-ignore
import UsFlagSvg from '../../../../assets/img/us.svg';
// END IMPORT IMAGE ZONE

// IMPORT INTERFACE ZONE
import {
    ICreateUserData,
    ISimpleModalParams,
    ICreateUserAccountResponse,
    TPhoneCountryId
} from '../../../../commonInterface';
// IMPORT INTERFACE ZONE

// INTERFACE ZONE
interface IProps {
    showSimpleModal: (params: ISimpleModalParams) => void,
    translator: any,
}

interface IState {
    textInEmailInput: string,
    textInUserNameInput: string,
    textInUserFirstNameInput: string,
    selectedPhoneCountryId: TPhoneCountryId,
    textInUserPhoneNumberInput: string,
    textInPasswordInput: string,
    textInConfirmPasswordInput: string,
    areGivenPasswordsSimilar: boolean,
}

// END INTERFACE ZONE

export default class SignInForm extends ComponentBase<IProps, IState> {

    protected componentId = 'signInForm';

    constructor(props: IProps) {
        super(props);

        this.state = {
            textInEmailInput: '',
            textInUserNameInput: '',
            textInUserFirstNameInput: '',
            selectedPhoneCountryId: 'fr',
            textInUserPhoneNumberInput: '',
            textInPasswordInput: '',
            textInConfirmPasswordInput: '',
            areGivenPasswordsSimilar: false,
        };
    }

    protected onEmailInputChange = (event) => {
        this.setState({
            textInEmailInput: event.target.value,
        });
    };

    protected onPasswordInputChange = (event) => {
        this.setState({
            textInPasswordInput: event.target.value,
        }, () => {
            this.updatePasswordInputsStatus();
        });
    };

    protected onUserNameInputChange = (event) => {
        this.setState({
            textInUserNameInput: event.target.value,
        });
    };

    protected onPhoneNumberInputChange = (event) => {
        this.setState({
            textInUserPhoneNumberInput: event.target.value,
        });
    }
    protected onUserFirstNameInputChange = (event) => {
        this.setState({
            textInUserFirstNameInput: event.target.value,
        });
    };


    protected setPhoneCountryId = (phoneCountryId: TPhoneCountryId) => {
        this.setState({
            selectedPhoneCountryId: phoneCountryId,
        });
    };

    protected getSelectedPhoneCountryIdRender = (phoneCountryId: TPhoneCountryId) => {
        switch (phoneCountryId) {
            case 'fr':
                return (
                    <span className={"phone-country-id"}>
                        <FrFlagSvg /> (+33)
                    </span>
                );

            case 'en':
                return (
                    <span className={"phone-country-id"}>
                        <UsFlagSvg /> (+1)
                    </span>
                );

            case 'de':
                return (
                    <span className={"phone-country-id"}>
                        <DeFlagSvg /> (+44)
                    </span>
                );
        }
    }

    protected getPhoneNumberWithId = () => {
        switch (this.state.selectedPhoneCountryId) {
            case 'fr':
                return "0033" + this.state.textInUserPhoneNumberInput;

            case 'en':
                return "001" + this.state.textInUserPhoneNumberInput;

            case 'de':
                return "0048" + this.state.textInUserPhoneNumberInput;
        }
    }

    protected onConfirmPasswordInputChange = (event) => {
        this.setState({
            textInConfirmPasswordInput: event.target.value,
        }, () => {
            this.updatePasswordInputsStatus();
        });
    };

    protected areGivenPasswordsNotSimilar(): boolean {
        const textInConfirmPasswordInput = this.state.textInConfirmPasswordInput;

        if (!Helper.isStringEmpty(textInConfirmPasswordInput) && textInConfirmPasswordInput !== this.state.textInPasswordInput) {
            return true;
        }

        return false;
    }

    protected updatePasswordInputsStatus = () => {
        this.setState({
            areGivenPasswordsSimilar: this.areGivenPasswordsNotSimilar(),
        })
    }

    protected onSignInBtnClick = () => {
        if (this.areInputsValid()) {
            const userData = this.getUserData();
            ApiCmds.createUserAccount(userData, (result) => {
                this.onSignInBtnClickCallback(result)
            });
        }
    };

    protected getUserData(): ICreateUserData {
        return {
            email: this.state.textInEmailInput,
            name: this.state.textInUserNameInput,
            first_name: this.state.textInUserFirstNameInput,
            phone: this.getPhoneNumberWithId(),
            password: Helper.hashStringMd5(this.state.textInPasswordInput),
        }
    }

    protected onSignInBtnClickCallback(result: ICreateUserAccountResponse) {
        if (result.success) {
            if (result.response.hasUserBeenCreated) {
                this.props.showSimpleModal({
                    type: 'success',
                    message: this.props.translator.checkout_email,
                })
            } else {
                this.props.showSimpleModal({
                    type: 'danger',
                    message: this.props.translator.email_already_exist,
                })
            }
        }
    }

    protected onKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.onSignInBtnClick();
        }
    };

    protected areInputsValid(): boolean {
        if (!Helper.checkEmail(this.state.textInEmailInput)) {
            this.props.showSimpleModal({
                type: 'danger',
                message: this.props.translator.enter_valid_email,
            })

            return false;
        }

        if (Helper.isStringEmpty(this.state.textInUserNameInput)
            || Helper.isStringEmpty(this.state.textInUserFirstNameInput)) {
            this.props.showSimpleModal({
                type: 'danger',
                message: "Veuillez rentrer un nom et un prénom valides",
            })

            return false;
        }

        if (this.state.textInPasswordInput !== this.state.textInConfirmPasswordInput) {
            this.props.showSimpleModal({
                type: 'danger',
                message: this.props.translator.passwords_not_same,
            })

            return false;
        }

        return true;
    }

    render() {
        return (
            <div id={this.setComponentId()} className="margin-side-xxl login-container page-transition">
                <div className="login-form-1">
                    <h3>{this.props.translator.signIn} :</h3>
                    <form>

                        <div id={this.setIdTo('nameInputsCtn')} className="">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                        <IconHandler icon="user" />
                                    </span>
                                </div>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder={"Votre nom *"}
                                    value={this.state.textInUserNameInput}
                                    onChange={this.onUserNameInputChange}
                                    onKeyPress={this.onKeyPress}
                                />
                            </div>

                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                        <IconHandler icon="user" />
                                    </span>
                                </div>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder={"Votre prénom *"}
                                    value={this.state.textInUserFirstNameInput}
                                    onChange={this.onUserFirstNameInputChange}
                                    onKeyPress={this.onKeyPress}
                                />
                            </div>
                        </div>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <button className="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {/* TODO:Localise */}
                                    {this.getSelectedPhoneCountryIdRender(this.state.selectedPhoneCountryId)}
                                </button>
                                <div className="dropdown-menu">
                                    <a
                                        className="dropdown-item" href="#"
                                        onClick={() => this.setPhoneCountryId('fr')}
                                    >
                                        {this.getSelectedPhoneCountryIdRender('fr')}
                                    </a>
                                    <a
                                        className="dropdown-item"
                                        onClick={() => this.setPhoneCountryId('de')}
                                    >
                                        {this.getSelectedPhoneCountryIdRender('de')}
                                    </a>
                                    <a
                                        className="dropdown-item"
                                        onClick={() => this.setPhoneCountryId('en')}
                                    >
                                        {this.getSelectedPhoneCountryIdRender('en')}
                                    </a>
                                </div>
                            </div>
                            <input
                                type="text"
                                className="form-control"
                                placeholder={"Votre numéro de téléphone *"}
                                value={this.state.textInUserPhoneNumberInput}
                                onChange={this.onPhoneNumberInputChange}
                                onKeyPress={this.onKeyPress}
                            />
                        </div>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="envelope" />
                                </span>
                            </div>
                            <input
                                type="email"
                                autoComplete="username"
                                className="form-control"
                                placeholder={this.props.translator.inputEmail}
                                value={this.state.textInEmailInput}
                                onChange={this.onEmailInputChange}
                                onKeyPress={this.onKeyPress}
                            />
                        </div>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="lock" />
                                </span>
                            </div>
                            <input
                                type="password"
                                autoComplete="new-password"
                                className="form-control"
                                placeholder={this.props.translator.inputPwd}
                                value={this.state.textInPasswordInput}
                                onChange={this.onPasswordInputChange}
                                onKeyPress={this.onKeyPress}
                            />
                        </div>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="lock" />
                                </span>
                            </div>
                            <input
                                type="password"
                                autoComplete="new-password"
                                className={"form-control " + (this.state.areGivenPasswordsSimilar ? "invalid-input" : "")}
                                placeholder="Your Password..."
                                value={this.state.textInConfirmPasswordInput}
                                onChange={this.onConfirmPasswordInputChange}
                                onKeyPress={this.onKeyPress}
                            />
                        </div>

                        <div className="form-group flex align-center">
                            <div
                                onClick={this.onSignInBtnClick}
                                className="btnSubmit"
                            >{this.props.translator.submit}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
