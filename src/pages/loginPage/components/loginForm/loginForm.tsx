import * as React from 'react';
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT HELPER ZONE
import Helper from '../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT COOKIEHANDLER ZONE
import { CookieHandler } from '../../../../assets/tools/storageHandler/storageHandler';
// END IMPORT COOKIEHANDLER ZONE

// IMPORT STYLES ZONE
import './loginForm.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACES ZONE
import {
    IIsUserData,
    ISimpleModalParams,
    TPages,
    IIsUserResponse,
    TPopups
} from '../../../../commonInterface';
// END IMPORT INTERFACES ZONE


interface IProps {
    goToPage: (pageName: TPages) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    translator: any,
    openPopup: (popupId: TPopups) => void,
}

interface IState {
    textInEmailInput: string,
    textInPasswordInput: string,
}


export default class LoginForm extends ComponentBase<IProps, IState> {

    protected componentId = 'loginForm';

    constructor(props: IProps) {
        super(props);

        this.state = {
            textInEmailInput: '',
            textInPasswordInput: '',
        };
    }

    protected onEmailInputChange = (event) => {
        this.setState({
            textInEmailInput: event.target.value,
        });
    };

    protected onPasswordInputChange = (event) => {
        this.setState({
            textInPasswordInput: event.target.value,
        });
    };

    protected onKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.onSubmit();
        }
    };

    protected onSubmit = () => {
        if (this.areInputsValid()) {
            const userData = this.getUserData();
            ApiCmds.isUser(userData,
                (result) => {
                    this.onIsUserCallback(result);
                }
            )
        }
    }

    protected onIsUserCallback(result: IIsUserResponse): void {
        if (result.success) {
            if (result.response.isUser) {
                CookieHandler.setCookie("token", result.response.token, 30);
                this.props.goToPage("mainPage");
            } else {
                this.props.showSimpleModal({
                    type: 'warning',
                    message: this.props.translator.username_or_password_incorrect,
                });
            }
        }
    }

    protected getUserData(): IIsUserData {
        return {
            email: this.state.textInEmailInput,
            password: Helper.hashStringMd5(this.state.textInPasswordInput),
        }
    }

    protected areInputsValid(): boolean {
        if (!Helper.checkEmail(this.state.textInEmailInput)) {
            this.props.showSimpleModal({
                type: 'danger',
                message: this.props.translator.enter_valid_email,
            });

            return false;
        }
        return true;
    }

    render() {
        return (
            <div id={this.setComponentId()} className="margin-side-xxl login-container page-transition">
                <div className="login-form-1">
                    <h3>{this.props.translator.login} : </h3>
                    <form>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="envelope" />
                                </span>
                            </div>
                            <input
                                type="email"
                                autoComplete="username"
                                className="form-control"
                                value={this.state.textInEmailInput}
                                onChange={this.onEmailInputChange}
                                onKeyPress={this.onKeyPress}
                                placeholder={this.props.translator.inputEmail}
                            />
                        </div>

                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">
                                    <IconHandler icon="lock" />
                                </span>
                            </div>
                            <input
                                type="password"
                                autoComplete="password"
                                className="form-control"
                                value={this.state.textInPasswordInput}
                                onChange={this.onPasswordInputChange}
                                onKeyPress={this.onKeyPress}
                                placeholder={this.props.translator.inputPwd}
                            />
                        </div>

                        <p className="forget-pwd clickable" onClick={() => { this.props.openPopup('forgetPwd') }}>
                            {this.props.translator.forget_Password}
                        </p>

                        <div className="form-group flex align-center">
                            <div
                                onClick={() => this.onSubmit()}
                                className="btnSubmit">
                                {this.props.translator.submit}
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        );
    }
}
