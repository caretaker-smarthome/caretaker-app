import * as React from 'react';

// IMPORT HELPER ZONE
import Helper from '../../helper';
// END IMPORT HELPER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

import WebsocketHandler from '../../core/api/websocketHandler';

// IMPORT COOKIEHANDLER ZONE
import { CookieHandler } from '../../assets/tools/storageHandler/storageHandler';
// END IMPORT COOKIEHANDLER ZONE

// IMPORT APP SETTINGS ZONE
import AppSettings from '../../appSettings';
// END IMPORT APP SETTINGS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize'
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './mainPage.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../assets/uiComponents/base/page/pageBase';
import Navbar from '../../assets/uiComponents/navbar/navbar';
import TabBar from '../../assets/uiComponents/tabBar/tabBar';
import CameraCapturePopup from './components/popups/cameraCapture/cameraCapture';
import FireAlertPopup from './components/popups/fireAlert/fireAlert';
import MotionDetectionAlertPopup from './components/popups/motionDetectionAlert/motionDetectionAlert';
// END IMPORT COMPONENTS ZONE

// IMPORT PAGES ZONE
import SponsorsPage from "./pages/sponsorsPage/sponsorsPage";
import QuickActionPage from "./pages/quickActionsPage/quickActionPage";
import SettingsPage from './pages/settingsPage/settingsPage';
import MyHomePage from './pages/myHomePage/myHomePage';
// END IMPORT PAGES ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TPages,
    TLanguages,
    IGetUserDataResponse,
    TPagesInMain,
    IUserData,
    IHome,
} from "../../commonInterface";
// END IMPORT INTERFACE ZONE

interface IProps {
    goToPage: (pageName: TPages) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    setLanguage: (language: TLanguages) => void,
    showSplashscreenPage: (showOrHide: boolean, ) => void,
    language: TLanguages,
}

interface IState {
    pageToShow: TPagesInMain,
    connectedHome: {
        wsKey: string,
        tag: string
        components: {},
    },
    userData: IUserData,
    userToken: string,
    userPseudo: string,
    userEmail: string,
    userAvatarLink: string,
    homeWS: WebsocketHandler
}


export default class MainPage extends PageBase<IProps, IState> {

    protected componentId = 'mainPage';
    protected userStartSeeSplashscreen: number;
    // protected SPLASHSCREEN_LIFE_DURATION: number = 900; // Minimum time the splashscreen should be visible to the user in ms
    protected SPLASHSCREEN_LIFE_DURATION: number = 0; // TODO: Remove this line for production

    constructor(props: IProps) {
        super(props);

        this.state = {
            pageToShow: 'myHomePage',
            connectedHome: {
                wsKey: '',
                tag: '',
                components: {},
            },
            homeWS: null,
            userData: {
                email: '',
                name: '',
                first_name: '',
                homes: [],
            },
            userToken: '',
            userPseudo: '',
            userEmail: '',
            userAvatarLink: '',
        };
    }

    protected init(): void {
        this.showLoadingAnimation();
        this.checkCookie();
        this.initHomeWs();
        this.initUI();
    }

    protected initUI(): void { }

    protected goToPageInMain = (pageName: TPagesInMain): void => {
        this.setState({
            pageToShow: pageName,
        })
    };

    protected getToLoginPageOnError(): void {
        this.hideLoadingAnimation();
        this.props.goToPage('loginPage');
    }

    protected checkCookie() {
        const token = CookieHandler.getCookie('token');

        if (!Helper.isStringEmpty(token)) {
            this.getUserData();
        } else {
            this.getToLoginPageOnError();
        }
    }

    protected initHomeWs = (): void => {
        if (this.state.connectedHome.wsKey !== '') {
            this.setState((prevState: IState) => ({
                // TODO: Connect to home key
                homeWS: new WebsocketHandler("homews/" + this.state.connectedHome.wsKey),
            }), this.onHomeWsOpen);
        }
    }

    protected onHomeWsOpen = (): void => {
        this.state.homeWS.onRequest('component/:components', this.onGetHomeComponents);
        this.state.homeWS.onRequest('identification/requested', this.identifyClient);
        this.state.homeWS.onRequest('smokeDetection/true', this.onSmokeDetection);
        this.state.homeWS.onRequest('motionDetection/true', this.onMotionDetection);
        this.state.homeWS.onRequest('identification/success', () => {
            $('#homeConnexionSuccesstoast').toast('show');
        });
    }

    protected onGetHomeComponents = (variables: any): void => {
        this.setState((prevState: IState) => ({
            connectedHome: {
                ...prevState.connectedHome,
                components: JSON.parse(decodeURIComponent(variables['components'])),
            }
        }));
    }

    private onMotionDetection = (): void => {
        this.openPopup('motionDetectionAlertPopup');
    }

    private onSmokeDetection = (): void => {
        this.openPopup('fireAlertPopup');
    }

    protected getUserData = (): void => {
        const token = {
            token: CookieHandler.getCookie('token'),
        };

        ApiCmds.getUserData(token, (result: IGetUserDataResponse) => {
            this.onGetUserDataCallback(result);
        })
    }

    protected showLoadingAnimation = (): void => {
        this.userStartSeeSplashscreen = Helper.getTimestamp();
        this.props.showSplashscreenPage(true);
    }

    protected hideLoadingAnimation = (): void => {
        const remainingVisibilityTime = Helper.getTimestamp() - this.userStartSeeSplashscreen + this.SPLASHSCREEN_LIFE_DURATION;

        setTimeout(() => {
            this.props.showSplashscreenPage(false);
        }, remainingVisibilityTime);
    }

    protected connectToHome = (homeWsKey: string, homeTag: string): void => {
        if (homeWsKey !== this.state.connectedHome.wsKey) {
            this.setState((prevState: IState) => ({
                connectedHome: {
                    ...prevState.connectedHome,
                    wsKey: homeWsKey,
                    tag: homeTag,
                }
            }), (): void => {
                this.initHomeWs();
            });
        }
    }

    protected identifyClient = (): void => {
        this.state.homeWS.sendRequest('server/identification/test123');
    }

    protected onGetUserDataCallback = (result: IGetUserDataResponse): void => {
        if (result.success) {
            const response = result.response;

            this.setUserToken();
            this.setUserLang(response.lang);

            this.setState((prevState: IState) => ({
                userData: {
                    ...prevState.userData,
                    name: response.pseudo,
                    first_name: response.pseudo,
                    avatarLink: response.avatarLink,
                    email: response.email,
                    homes: response.homes,
                },
                userPseudo: response.pseudo,
                userEmail: response.email,
                userAvatarLink: response.avatarLink,
            }), () => {
                this.hideLoadingAnimation();
                this.setUserCurrentHomeConnexion(response.currents_home_tag);
            });
        } else {
            this.getToLoginPageOnError();
        }
    }

    protected setUserCurrentHomeConnexion = (homeTag: string): void => {
        this.state.userData.homes.forEach((home: IHome) => {
            if (home.tag === homeTag) {
                this.connectToHome(home.ws_key, home.tag);
            }
        });
    }

    protected setUserToken = (): void => {
        this.setState({
            userToken: CookieHandler.getCookie('token'),
        });
    }

    protected setUserLang = (lang: TLanguages): void => {
        this.props.setLanguage(lang);
    }

    protected logoutUser = (): void => {
        CookieHandler.deleteCookie('token');
        this.props.goToPage('loginPage');
    }


    protected navbarRender(): React.ReactNode {
        return (
            <Navbar
                goToPage={this.goToPageInMain}
                setLanguage={this.props.setLanguage}
                language={this.props.language}
                userPseudo={this.state.userPseudo}
                userAvatarLink={this.state.userAvatarLink}
                logoutUser={this.logoutUser}
            />
        )
    }

    protected tabBarRender(): React.ReactNode {
        return (
            <TabBar
                goToPage={this.goToPageInMain}
                currentPage={this.state.pageToShow}
                showSimpleModal={this.props.showSimpleModal}
                isConnectedToHome={this.state.connectedHome.wsKey !== ''}
            />
        )
    }

    protected sponsorsPageRender(): React.ReactNode {
        return (
            <SponsorsPage
                goToPage={this.goToPageInMain}
                language={this.props.language}
                showSimpleModal={this.props.showSimpleModal}
            />
        )
    }

    protected myHomePageRender(): React.ReactNode {
        return (
            <MyHomePage
                goToPage={this.goToPageInMain}
                language={this.props.language}
                showSimpleModal={this.props.showSimpleModal}
                homes={this.state.userData.homes}
                homeWs={this.state.homeWS}
                connectToHome={this.connectToHome}
            />
        )
    }

    protected quickActionPageRender(): React.ReactNode {
        return (
            <QuickActionPage
                goToPage={this.goToPageInMain}
                language={this.props.language}
                showSimpleModal={this.props.showSimpleModal}
                homeWS={this.state.homeWS}
                components={this.state.connectedHome.components}
                openPopup={this.openPopup}
            />
        )
    }

    protected settingsPageRender(): React.ReactNode {
        return (
            <SettingsPage
                goToPage={this.goToPageInMain}
                language={this.props.language}
                showSimpleModal={this.props.showSimpleModal}
            />
        )
    }

    private popupsRender(): React.ReactNode {
        return (
            <div className="popups-ctn">
                <CameraCapturePopup
                    homeWS={this.state.homeWS}
                    goToPage={this.goToPageInMain}
                    language={this.props.language}
                    showSimpleModal={this.props.showSimpleModal}
                ></CameraCapturePopup>
                <FireAlertPopup
                    goToPage={this.goToPageInMain}
                    language={this.props.language}
                    showSimpleModal={this.props.showSimpleModal}
                ></FireAlertPopup>
                <MotionDetectionAlertPopup
                    goToPage={this.goToPageInMain}
                    language={this.props.language}
                    showSimpleModal={this.props.showSimpleModal}
                ></MotionDetectionAlertPopup>
            </div>
        )
    }


    render(): React.ReactNode {
        return (
            <div id={this.setComponentId()}>
                {this.navbarRender()}
                {this.tabBarRender()}

                <div id={this.setIdTo("pages")} className="with-scrollbar">
                    {
                        (() => {
                            switch (this.state.pageToShow) {

                                case 'sponsorsPage':
                                    return this.sponsorsPageRender();
                                case 'myHomePage':
                                    return this.myHomePageRender();
                                case 'quickActionPage':
                                    return this.quickActionPageRender();
                                case 'settingsPage':
                                    return this.settingsPageRender();

                            }
                        })()
                    }

                    <div id="homeConnexionSuccesstoast" className="toast" data-autohide="true" data-delay="3000" role="alert" aria-live="assertive" aria-atomic="true">
                        <div className="toast-header">
                            <strong className="mr-auto">Home connection</strong>
                            <small>1 min ago</small>
                            <button type="button" className="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="toast-body">
                            Your connection to the home <b>#{this.state.connectedHome.tag}</b> has succeeded
                    </div>
                    </div>
                </div>

                {this.popupsRender()}
            </div>
        );
    }
}
