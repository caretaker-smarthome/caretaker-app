import * as React from 'react';
import IconHandler from '../../../../../assets/uiComponents/iconHandler/iconHandler';


// IMPORT HELPER ZONE
import Helper from '../../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './motionDetectionAlert.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PopupBase from '../../../../../assets/uiComponents/base/popup/popupBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TLanguages,
    TPagesInMain,
} from "../../../../../commonInterface";
// END IMPORT INTERFACE ZONE

interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
}

interface IState { }


export default class MotionDetectionAlertPopup extends PopupBase<IProps, IState> {

    protected componentId = 'motionDetectionAlertPopup';
    protected popupName = 'Motion detection';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected init(): void { }

    protected clear(): void { }

    protected popupBodyRender(): React.ReactNode {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} className="popup-body card card-body">
                <h1>Motion detected in your house <IconHandler icon="running" /></h1>
            </div>
        )
    }
}
