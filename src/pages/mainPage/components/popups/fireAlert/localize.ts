const LOCALIZE = {
    de: {
        capture_image: 'Machen Sie ein Foto'
    },
    fr: {
        capture_image: 'Prendre un photo'
    },
    en: {
        capture_image: 'Take a photo'
    },
};

export default LOCALIZE;