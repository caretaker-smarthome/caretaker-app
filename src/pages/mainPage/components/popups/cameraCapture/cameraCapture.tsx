import * as React from 'react';
import IconHandler from '../../../../../assets/uiComponents/iconHandler/iconHandler';


// IMPORT HELPER ZONE
import Helper from '../../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './cameraCapture.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PopupBase from '../../../../../assets/uiComponents/base/popup/popupBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TLanguages,
    TPagesInMain,
} from "../../../../../commonInterface";
import WebsocketHandler from '~core/api/websocketHandler';
// END IMPORT INTERFACE ZONE

interface IProps {
    homeWS: WebsocketHandler;
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
}

interface IState {
    imgRequestSend: boolean;
}


export default class CameraCapturePopup extends PopupBase<IProps, IState> {

    protected componentId = 'cameraCapturePopup';
    protected popupName = 'Capture camera';

    constructor(props: IProps) {
        super(props);

        this.state = {
            imgRequestSend: false,
        };
    }

    protected init(): void { }

    protected clear(): void {
        $(this.getId('image')).html('');
    }

    private captureImage = (): void => {
        if (!this.state.imgRequestSend) {
            this.initHomeWsRequest();
        }

        this.props.homeWS.sendRequest('home/camera/capture/true');
    }

    private initHomeWsRequest = (): void => {
        this.props.homeWS.onRequest('camera/image/:imgHasBytes', this.onGetImg);

        this.setState((prevState: IState) => ({
            imgRequestSend: true,
        }));
    }

    private onGetImg = (variables): void => {
        var img = new Image();
        img.src = "data:image/jpg;base64," + decodeURIComponent(variables['imgHasBytes']);

        $(this.getId('image')).html(img);
    }

    protected popupBodyRender(): React.ReactNode {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} className="popup-body card card-body">
                <button type="button" className="btn btn-secondary" onClick={() => this.captureImage()}>
                    <IconHandler icon="camera-retro" className="with-pr" />
                    {localize.capture_image}
                </button>

                <div id={this.setIdTo('image')}></div>
            </div>
        )
    }
}
