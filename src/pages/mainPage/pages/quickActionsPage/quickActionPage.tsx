import * as React from 'react';
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

import WebsocketHandler from '../../../../core/api/websocketHandler';

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './quickActionPage.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../../../assets/uiComponents/base/page/pageBase';
import SwitchCheckbox from '../../../../assets/uiComponents/switchCheckbox/switchCheckbox';
import HomeLocker from '../../../../core/components/homeLocker/homeLocker';
import FireplaceInput from '../../../../core/components/fireplaceInput/fireplaceInput';
import CameraInput from '../../../../core/components/cameraInput/cameraInput';
import HumidityDisplay from '../../../../core/components/humidityDisplay/humidityDisplay';
import TemperatureDisplay from '../../../../core/components/temperatureDisplay/temperatureDisplay';
import LightInput from '../../../../core/components/lightInput/lightInput';
// END IMPORT COMPONENTS ZONE

// IMPORT COMMON_LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../commonLocalize';
// END IMPORT COMMON_LOCALIZE ZONE

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    ISimpleModalParams,
    TPagesInMain,
    TPopups
} from '../../../../commonInterface';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    goToPage: (pageName: TPagesInMain) => void;
    showSimpleModal: (params: ISimpleModalParams) => void;
    language: TLanguages;
    homeWS: WebsocketHandler;
    components: {};
    openPopup: (popupId: TPopups) => void
}

interface IState {
    isHomeLocked: boolean,
}

// END INTERFACES ZONE


export default class QuickActionPage extends PageBase<IProps, IState> {

    protected componentId = 'quickActionPage';

    constructor(props: IProps) {
        super(props);

        this.state = {
            isHomeLocked: false,
        };
    }

    protected init(): void {
        this.props.homeWS.sendRequest(`home/humidityTemperaturesSensor1/read/start`);
    };

    protected clear(): void { };

    protected swapeHomeLockStatus = (): void => {
        this.props.homeWS.send({
            requests:
                [
                    {
                        to: 'home',
                        request: this.state.isHomeLocked ? 'home-lock' : 'home-unlock', // TODO: Write lock home request
                    }
                ],
        });

        this.setState((prevState: IState) => ({
            isHomeLocked: !this.state.isHomeLocked,
        }));
    };

    protected homeComponentsInputs = (): React.ReactNode[] => {
        let componentsInputs: React.ReactNode[] = [];

        let i = 0;
        for (const componentName in this.props.components) {
            switch (this.props.components[componentName]['type']) {
                case 'fireplace':
                    componentsInputs.push(
                        <FireplaceInput
                            key={i}
                            componentName={componentName}
                            homeWS={this.props.homeWS}
                            className="components col-12 col-lg-5 m-2"
                        />
                    )
                    break;

                case 'camera':
                    componentsInputs.push(
                        <CameraInput
                            key={i}
                            componentName={componentName}
                            homeWS={this.props.homeWS}
                            className="components col-12 col-lg-5 m-2"
                            openCameraPopup={() => this.props.openPopup('cameraCapturePopup')}
                        />
                    )
                    break;

                case 'humidityTemperaturesSensor':
                    componentsInputs.push(
                        <TemperatureDisplay
                            key={i}
                            componentName={componentName}
                            homeWS={this.props.homeWS}
                            className="components col-12 col-lg-5 m-2"
                        />,
                        <HumidityDisplay
                            key={i}
                            componentName={componentName}
                            homeWS={this.props.homeWS}
                            className="components col-12 col-lg-5 m-2"
                        />
                    )
                    break;

                case 'leds':
                    componentsInputs.push(
                        <LightInput
                            key={i}
                            componentName={componentName}
                            homeWS={this.props.homeWS}
                            className="components col-12 col-lg-5 m-2"
                        />
                    )
                    break;
            }

            i++;
        }

        return componentsInputs;
    }

    render = (): React.ReactNode => {
        const commonLocalize = COMMON_LOCALIZE[this.props.language];
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} className="page page-transition">
                <h1>
                    <IconHandler icon="bolt" />
                    Quick Action Page
                </h1>
                <hr />

                <section id={this.setIdTo('quickActionsCtn')} className="container">
                    <div className="row  d-flex justify-content-center">
                        <HomeLocker
                            componentName="locker"
                            homeWS={this.props.homeWS}
                            className="col-12 col-lg-5 m-2"
                        ></HomeLocker>
                        {this.homeComponentsInputs().map(element => {
                            return element;
                        })}
                    </div>

                </section>

            </div>
        );
    }
}
