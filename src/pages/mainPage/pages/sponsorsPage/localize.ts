const LOCALIZE = {
    'de': {
        subject: "Betreff...",
        message: "Nachricht...",

    },
    'fr': {
        subject: "Sujet...",
        message: "Message...",
    },

    'en': {
        subject: "Subject...",
        message: "Message...",
    },
};

export default LOCALIZE;