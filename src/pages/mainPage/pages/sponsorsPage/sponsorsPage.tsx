import * as React from 'react';
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './sponsorsPage.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../../../assets/uiComponents/base/page/pageBase';
// END IMPORT COMPONENTS ZONE

// IMPORT COMMON_LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../commonLocalize';
// END IMPORT COMMON_LOCALIZE ZONE

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    ISimpleModalParams,
    TPagesInMain
} from '../../../../commonInterface';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
}

interface IState { }

// END INTERFACES ZONE


export default class SponsorsPage extends PageBase<IProps, IState> {

    protected componentId = 'sponsorsPage';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected init(): void { };

    protected clear(): void { };

    render() {
        const commonLocalize = COMMON_LOCALIZE[this.props.language];
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} className="page page-transition">
                <h1>SponsorsPage</h1>
                {localize.subject}
            </div>
        );
    }
}
