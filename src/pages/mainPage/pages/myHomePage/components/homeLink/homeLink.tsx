import * as React from 'react';

// IMPORT HELPER ZONE
import Helper from '../../../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT STYLES ZONE
import './homeLink.scss';
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
// import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT IMAGE ZONE
// END IMPORT IMAGE ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../../../../assets/uiComponents/base/componentBase';
import IconHandler from '../../../../../../assets/uiComponents/iconHandler/iconHandler';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACES ZONE
// END IMPORT INTERFACES ZONE


interface IProps {
    tag: string,
    ws_key: string,
    connectToHome: (homeWsKey: string, homeTag: string) => void,
}

interface IState {
}


export default class HomeLink extends ComponentBase<IProps, IState> {

    protected componentId = 'homeLink';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected init(): void { }

    protected clear(): void { }


    render(): React.ReactNode {
        return (
            <div
                id={this.setComponentId()}
                className="clickable-effect"
                onClick={() => { this.props.connectToHome(this.props.ws_key, this.props.tag) }}
            >
                <div className="img">
                    <IconHandler icon="home" />
                </div>
                <h3>#{this.props.tag}</h3>
            </div>
        );
    }
}
