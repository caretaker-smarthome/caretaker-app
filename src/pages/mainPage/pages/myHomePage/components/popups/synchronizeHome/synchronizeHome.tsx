import * as React from 'react';
import IconHandler from '../../../../../../../assets/uiComponents/iconHandler/iconHandler';


// IMPORT HELPER ZONE
import Helper from '../../../../../../../helper';
// END IMPORT HELPER ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../../../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './synchronizeHome.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PopupBase from '../../../../../../../assets/uiComponents/base/popup/popupBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
import {
    ISimpleModalParams,
    TLanguages,
    TPagesInMain,
    ISynchronizeHomeData,
    ISynchronizeHomeResponse,
} from "../../../../../../../commonInterface";
// END IMPORT INTERFACE ZONE

interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
}

interface IState {
    textInHomeTagInput: string,
    textInHomePasswordInput: string,
}


export default class SynchronizeHomePopup extends PopupBase<IProps, IState> {

    protected componentId = 'synchronizeHomePopup';
    protected popupName = 'Synchronize my Home';

    constructor(props: IProps) {
        super(props);

        this.state = {
            textInHomeTagInput: '',
            textInHomePasswordInput: '',
        };
    }

    protected init(): void { }

    protected clear(): void {

        this.setState((prevState: IState) => ({
            textInHomeTagInput: '',
        }))

    }

    protected onHomeTagInputChange = (event): void => {
        this.setState({
            textInHomeTagInput: event.target.value,
        });
    }

    protected onHomePasswordInputChange = (event): void => {
        this.setState({
            textInHomePasswordInput: event.target.value,
        });
    }

    protected onKeyPress = (event): void => {
        if (event.key === 'Enter') {
            this.onSubmitBtnClick();
        }
    }

    protected areInputsValid = (): boolean => {
        if (this.state.textInHomeTagInput === '' || this.state.textInHomePasswordInput === '') {
            // TODO: Show error modal
            return false;
        }

        return true;
    }

    protected onSubmitBtnClick() {
        if (this.areInputsValid()) {
            ApiCmds.synchronizeHome(this.getUserData(),
                (result: ISynchronizeHomeResponse) => {
                    this.onSubmitBtnClickCallback(result);
                }
            );
        }
    }

    protected getUserData = (): ISynchronizeHomeData => {
        return {
            home_tag: this.state.textInHomeTagInput,
            home_password: Helper.hashStringMd5(this.state.textInHomePasswordInput),
        }
    }

    protected onSubmitBtnClickCallback = (result: ISynchronizeHomeResponse): void => {
        if (result.success) {
            this.props.showSimpleModal({
                type: 'success',
                message: 'You are now connected to #' + result.response.home_tag,
            });
        } else {
            this.props.showSimpleModal({
                type: 'danger',
                message: result.msg,
            });
        }
    }

    protected popupBodyRender(): React.ReactNode {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} className="popup-body card card-body">

                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">
                            <IconHandler icon="tag" />
                        </span>
                    </div>
                    <input type="text"
                        placeholder={localize.home_tag_input}
                        className="form-control"
                        value={this.state.textInHomeTagInput}
                        onChange={this.onHomeTagInputChange}
                    />
                </div>

                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">
                            <IconHandler icon="lock" />
                        </span>
                    </div>
                    <input type="password"
                        placeholder={localize.password}
                        className="form-control"
                        value={this.state.textInHomePasswordInput}
                        onChange={this.onHomePasswordInputChange}
                    />
                </div>

                <button type="button" className="btn btn-primary" onClick={() => this.onSubmitBtnClick()}>
                    <IconHandler icon="retweet" className="with-pr" />
                    {localize['submit']}
                </button>
            </div>
        )
    }
}
