const LOCALIZE = {
    de: {
        title: "Enter your email and check your account (don't forget to check in spam)",
        home_tag_input: "Home tag (bsp: home1234)",
        enter_valid_email: "Bitte geben Sie eine gültige E-Mail-Adresse ein!",
        submit: "Einreichen",
        forget_pwd_mail_send: "Die E-Mail zur Passwortwiederherstellung wurde Ihnen zugesandt !",
        password: "Home Passwort",
        synchronize: "Synchronisieren",
    },
    fr: {
        title: "Enter your email and check your account (don't forget to check in spam)",
        home_tag_input: "Home tag (ex: home1234)",
        enter_valid_email: "Veuillez entrer un email valide !",
        submit: "Soumettre",
        forget_pwd_mail_send: "L'email de récupération de mot de passe vous a été envoyé !",
        password: "Mot de passe de votre Home",
        synchronize: "Synchroniser",
    },

    en: {
        title: "Enter your email and check your account (don't forget to check in spam)",
        home_tag_input: "Home tag (ex: home1234)",
        enter_valid_email: "Please enter valid email !",
        submit: "Submit",
        forget_pwd_mail_send: "The password recovery email has been sent to you !",
        password: "Home password",
        synchronize: "Synchronize",
    },
};

export default LOCALIZE;