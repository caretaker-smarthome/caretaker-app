import * as React from 'react';
import IconHandler from '../../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

import WebsocketHandler from '../../../../core/api/websocketHandler';

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './myHomePage.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import PageBase from '../../../../assets/uiComponents/base/page/pageBase';
import HomeLink from './components/homeLink/homeLink';
import SynchronizeHomePopup from './components/popups/synchronizeHome/synchronizeHome';
// END IMPORT COMPONENTS ZONE

// IMPORT COMMON_LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../commonLocalize';
// END IMPORT COMMON_LOCALIZE ZONE

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    ISimpleModalParams,
    TPagesInMain,
    IHome
} from '../../../../commonInterface';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    language: TLanguages,
    homes: IHome[],
    homeWs: WebsocketHandler,
    connectToHome: (homeWsKey: string, homeTag: string) => void,
}

interface IState { }

// END INTERFACES ZONE


export default class MyHomePage extends PageBase<IProps, IState> {

    protected componentId = 'myHomePage';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected init(): void { };

    protected clear(): void { };

    protected myHomePageRender(): React.ReactNode {
        return (
            <React.Fragment>
                <div id={this.setIdTo('homesCtn')}>
                    {this.props.homes.map((home: IHome, i: number) => {
                        return (
                            <HomeLink
                                key={i}
                                ws_key={home.ws_key}
                                tag={home.tag}
                                connectToHome={this.props.connectToHome}
                            />
                        )
                    })}
                </div>
            </React.Fragment>
        )
    }

    protected synchronizeHomePageRender(): React.ReactNode {
        return (
            <div id={this.setIdTo('synchronizeHome')}>
                <h1 onClick={() => { this.openPopup('synchronizeHomePopup') }} className="clickable">
                    <IconHandler icon="plus-square" className="with-pr" />
                    Synchronize your Home
                </h1>
            </div>
        )
    }

    protected popupsRender(): React.ReactNode {
        return (
            <div className="popups-ctn">
                <SynchronizeHomePopup
                    goToPage={this.props.goToPage}
                    language={this.props.language}
                    showSimpleModal={this.props.showSimpleModal}
                />
            </div>
        )
    }


    render() {
        const commonLocalize = COMMON_LOCALIZE[this.props.language];
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} className="page page-transition">
                {this.props.homes.length ? this.myHomePageRender() : this.synchronizeHomePageRender()}

                {this.popupsRender()}
            </div>
        );
    }
}
