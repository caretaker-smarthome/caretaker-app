import { IconProp } from '@fortawesome/fontawesome-svg-core';

export interface IDimensions {
    width: number,
    height: number,
}

export interface IToken {
    token: string,
}

export interface IIsUserData {
    email: string,
    password: string,
}

export interface ISynchronizeHomeData {
    home_tag: string,
    home_password: string,
}

export interface ICreateUserData {
    email: string,
    name: string,
    first_name: string,
    phone: string,
    password: string,
}

export interface IForgetPwdData {
    email: string,
}

export interface IModalParams {
    title: string,
    body: string,
    okBtnText?: string,
    onOk: () => void,
}

export interface ISimpleModalParams {
    type: TSimpleModalType,
    message: string,
    noAutoHide?: boolean,
    isHtml?: boolean,
}

export interface IUserData {
    email: string,
    name: string,
    first_name: string,
    homes: IHome[],
}

export interface IHome {
    ws_key: string,
    tag: string,
}

// API INTERFACES
export interface ISetLangData {
    lang: TLanguages,
}

export interface IChangePwdData {
    token: string,
    pwd: string,
    newPwd: string,
}

export interface ISendEmailToAdminData extends IToken {
    subject: string,
    msg: string,
}

export interface IRequestResponse {
    success: boolean,
    msg?: string,
}

export interface ICreateUserAccountResponse extends IRequestResponse {
    response: {
        hasUserBeenCreated: boolean,
    }
}

export interface IIsUserResponse extends IRequestResponse {
    response: {
        isUser: boolean,
        token?: string,
    }
}

export interface ISynchronizeHomeResponse extends IRequestResponse {
    response: {
        home_tag: string,
        ws_key: string,
    }
}

export interface IIsTokenValidResponse extends IRequestResponse {
    response: {
        isTokenValid: boolean,
    }
}

export interface IGetUserDataResponse extends IRequestResponse {
    response: {
        lang: TLanguages,
        pseudo: string,
        email: string,
        avatarLink: string,
        homes: IHome[],
        currents_home_tag: string,
    }
}

export interface ISetLangResponse extends IRequestResponse {
    response: {
        hasLangBeChanged: boolean, // TODO: Check spelling
    }
}

export interface IChangePwdResponse extends IRequestResponse {
    response: {
        hasPwdBeChanged: boolean, // TODO: Check spelling
    }
}

export interface ISendEmailToAdminResponse extends IRequestResponse {
    response: {
        hasEmailBeSend: boolean, // TODO: Check spelling
    }
}

export interface ISendForgetPwdMailResponse extends IRequestResponse {
    response: {
        hasEmailBeSend: boolean, // TODO: Check spelling
    }
}
// END API INTERFACES

// TYPES ZONE
export type TPages = 'loginPage' | 'mainPage' | 'forgetPwdPage';
export type TPagesInMain = 'sponsorsPage' | 'quickActionPage' | 'settingsPage' | 'contactPage' | 'myHomePage';
export type TLanguages = 'en' | 'fr' | 'de';
export type TSimpleModalType = 'success' | 'warning' | 'danger';
export type TFontawesomeIconClass = IconProp;
export type TScreenOrientation = 'portrait' | 'landscape';
export type TConnexionStatus = 'online' | 'offline';
export type TComponent = 'fireplace' | 'humidityTemperaturesSensor' | 'smokeSensor';
export type TActionName = 'identification';
export type TPopups = TPopupsInLoginPage | TPopupsInMainPage;
export type TPhoneCountryId = 'en' | 'fr' | 'de';

export type TPopupsInLoginPage = 'forgetPwd';
export type TPopupsInMainPage = 'synchronizeHomePopup' | 'cameraCapturePopup' | 'fireAlertPopup' | 'motionDetectionAlertPopup';
// END TYPES ZONE