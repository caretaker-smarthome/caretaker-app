// IMPORT CREDENTIALS
import * as CREDENTIALS from './credentials.json';
// END IMPORT CREDENTIALS

// IMPORT APP SETTINGS ZONE
import AppSettings from '../../appSettings';
// END IMPORT APP SETTINGS ZONE

export default class _api {

    protected PATH_TO_API: string;

    constructor() {
        this.setPathToApi();
    }

    protected setPathToApi(): void {
        this.PATH_TO_API = AppSettings.settings.api[AppSettings.settings.api_to_use];
    }

    protected getCmdLink(cmdName: string): string {
        return this.PATH_TO_API + 'cmds/' + cmdName + '/';
    }

    protected uniformPostRequest(request: Object): Object {
        return {
            'header': {
                'private_key': CREDENTIALS.private_key,
            },
            'request': request,
        };
    }

    protected showLoadingAnimation(): void {
        $('#loadingAnimation').css({
            display: "flex",
        });
    }

    protected hideLoadingAnimation(): void {
        $('#loadingAnimation').css({
            display: "none",
        });
    }

    protected showServerErrorModal(): void {
        $('#serverErrorModal').modal();
    }

    protected hideServerErrorModal(): void {
        $('#serverErrorModal').modal('hide');
    }

    public postRequest(cmdName: string, request: Object, callbackFunction: (response: any) => void, withAnimation: boolean = true): void {
        if (withAnimation) {
            this.showLoadingAnimation();
        }

        $.ajax({
            type: 'POST',
            url: this.getCmdLink(cmdName),
            data: this.uniformPostRequest(request),
            dataType: 'JSON',
            success: (response) => {
                callbackFunction(response);

                this.hideLoadingAnimation();
            },
            error: () => {
                this.showServerErrorModal();
                this.hideLoadingAnimation();
            }
        });
    }
}
