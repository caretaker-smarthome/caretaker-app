// IMPORT CREDENTIALS
import * as CREDENTIALS from './credentials.json';
// END IMPORT CREDENTIALS

// IMPORT APP SETTINGS ZONE
import AppSettings from '../../appSettings';
// END IMPORT APP SETTINGS ZONE

import Helper from '../../helper';

// IMPORT INTERFACE ZONE
import { TActionName } from '../../commonInterface.js';
// END IMPORT INTERFACE ZONE

export default class WebsocketHandler {

    protected PATH_TO_WS: string;
    public isIdentified = false;

    protected connexion: WebSocket;
    protected onOpenCallback: () => void;
    protected onRequestCallbacks = {};

    constructor(wsPath: string) {
        this.setPathToWs(wsPath);
        this.connexion = new WebSocket(this.PATH_TO_WS);

        // Associate websocket events to callbacks
        this.connexion.onopen = (e) => { this._onOpen(e) };
        this.connexion.onclose = (e) => { this._onClose(e) };
        this.connexion.onmessage = (e) => { this._onMessage(e) };
        this.connexion.onerror = () => { this._onError };
    }

    protected setPathToWs(wsPath: string): void {
        this.PATH_TO_WS = AppSettings.settings.ws[AppSettings.settings.ws_to_use] + wsPath + '/';
    }

    protected _onOpen(e): void {
        console.log("Connection established with -> " + e.srcElement.url);
    }

    protected _onClose(e): void {
        if (e.wasClean) {
            alert(`[close] Connection closed cleanly, code=${e.code} reason=${e.reason}`);
        } else {
            this.showServerErrorModal();
        }
    }

    protected _onError = (): void => {
        this.showServerErrorModal();
    }

    protected _onMessage = (e): void => {
        this.onMessage(JSON.parse(e.data));
    }

    protected _send = (data: Object): void => {
        const uniformMessage = this.getUniformMessage(data);
        this.connexion.send(JSON.stringify(uniformMessage));
    }

    protected getUniformMessage(data: Object): Object {
        return {
            'header': {
                'sender_type': 'client',
                'private_key': '123Soleil',
            },
            'body': data,
        };
    }


    public onOpen = (callback: () => void): void => {
        this.onOpenCallback = callback;
    };

    public onMessage = (msg: Object): void => {
        const request = msg['body']['request'];

        // TODO: Handle in an internal API
        if (Helper.isSet(request)) {
            this.handleRequest(request);
        }
    };

    protected handleRequest(request: string): void {
        const splittedRequest = request.split('/');
        const subject = splittedRequest[0];
        const action = splittedRequest[1]

        for (const requestTemplate in this.onRequestCallbacks) {
            const splittedRequestTemplate = requestTemplate.split('/');

            let requestMatchRequestTemplate = true;
            let requestVariables = {};
            splittedRequestTemplate.forEach((element: string, i: number) => {
                if (splittedRequestTemplate[i].startsWith(':')) {
                    const requestVariable = splittedRequestTemplate[i].substring(1);
                    requestVariables[requestVariable] = splittedRequest[i];
                }
                else if (splittedRequest[i] !== splittedRequestTemplate[i]) {
                    requestMatchRequestTemplate = false;
                    // TODO: Break here loop
                }
            });

            if (requestMatchRequestTemplate) {
                // Execute all callbacks from matching request
                this.onRequestCallbacks[requestTemplate].forEach((callback) => {
                    callback(requestVariables);
                });
            }
        }
    }

    public send = (data: Object): void => {
        this._send(data);
    };

    public sendRequest = (request: string): void => {
        this.send({
            request: request,
        });
    }

    protected showServerErrorModal(): void {
        $('#serverErrorModal').modal();
    }

    protected hideServerErrorModal(): void {
        $('#serverErrorModal').modal('hide');
    }

    /**
    * Execute onRequestCallback when a request match requestTemplate
    *
    * @param requestTemplate - The request template to match (can contain variables like `:myVariable`)
    * @param onRequestCallback - The callback that will be call when there is a match (callback will get as parameter an object with the variable associated to his value like `{ 'myVariable': value }`)
    *
    */
    public onRequest(requestTemplate: string, onRequestCallback: (variables: any) => void): void {
        if (this.onRequestCallbacks[requestTemplate] === undefined) {
            this.onRequestCallbacks[requestTemplate] = [onRequestCallback];
        } else {
            this.onRequestCallbacks[requestTemplate].push(onRequestCallback);
        }
    }
}