import * as React from 'react';
import IconHandler from '../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './cameraInput.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import CameraIcon from './assets/camera.svg';
// END IMPORT IMAGE ZONE


// IMPORT INTERFACE ZONE
import {
    TLanguages,
} from '../../../commonInterface';
import WebsocketHandler from '../../../core/api/websocketHandler';
import Helper from '../../../helper';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    componentName: string;
    homeWS: WebsocketHandler;
    openCameraPopup: () => void;
    className?: string,
}

interface IState { }

// END INTERFACES ZONE


export default class cameraInput extends ComponentBase<IProps, IState> {

    protected componentId = 'camera';

    constructor(props: IProps) {
        super(props);

        this.state = { };
    }

    protected init = (): void => {
    };

    protected clear(): void { };

    protected getClassName = (): string => {
        const defaultClassAttr = 'cameraInput';

        if (Helper.isUndefined(this.props.className)) {
            return defaultClassAttr;
        }

        return defaultClassAttr + ' ' + this.props.className;
    }

    render(): React.ReactNode {
        return (
            <div id={this.setComponentId()} className={this.getClassName()} onClick={this.props.openCameraPopup}>
                <CameraIcon />
                <p>{this.props.componentName}</p>
            </div>
        );
    }
}
