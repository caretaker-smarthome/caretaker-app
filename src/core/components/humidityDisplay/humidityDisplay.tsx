import * as React from 'react';
import IconHandler from '../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './humidityDisplay.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import HumidityIcon from './assets/humidity.svg';
// END IMPORT IMAGE ZONE


// IMPORT INTERFACE ZONE
import {
    TLanguages,
} from '../../../commonInterface';
import WebsocketHandler from '../../../core/api/websocketHandler';
import Helper from '../../../helper';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    componentName: string;
    homeWS: WebsocketHandler;
    className?: string,
}

interface IState {
    humidityValue: number
}

// END INTERFACES ZONE


export default class humidityDisplay extends ComponentBase<IProps, IState> {

    protected componentId = 'humidity';

    constructor(props: IProps) {
        super(props);

        this.state = {
            humidityValue: 0,
        };
    }

    protected init = (): void => {
        this.props.homeWS.onRequest(`${this.props.componentName}/humidity/:humidity`, this.onNewHumidity)
    };

    private onNewHumidity = (variables: any): void => {
        this.setState((prevState: IState) => ({
            humidityValue: parseFloat(variables['humidity']),
        }));
    }

    protected clear(): void { };

    protected getClassName = (): string => {
        const defaultClassAttr = 'humidityDisplay';

        if (Helper.isUndefined(this.props.className)) {
            return defaultClassAttr;
        }

        return defaultClassAttr + ' ' + this.props.className;
    }

    render(): React.ReactNode {
        return (
            <div id={this.setComponentId()} className={this.getClassName()}>
                <HumidityIcon />
                <p className="value">{this.state.humidityValue}%</p>
                <p>{this.props.componentName}</p>
            </div>
        );
    }
}
