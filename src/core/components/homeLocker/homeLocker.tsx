import * as React from 'react';
import IconHandler from '../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './homeLocker.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE


// IMPORT INTERFACE ZONE
import {
    TLanguages,
} from '../../../commonInterface';
import WebsocketHandler from '~core/api/websocketHandler';
import Helper from '~helper';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    componentName: string;
    homeWS: WebsocketHandler;
    className?: string,
}

interface IState {
    isHomeLocked: boolean;
}

// END INTERFACES ZONE


export default class HomeLocker extends ComponentBase<IProps, IState> {

    protected componentId = 'homeLocker';

    constructor(props: IProps) {
        super(props);

        this.state = {
            isHomeLocked: false,
        };
    }

    protected init = (): void => {
        this.props.homeWS.onRequest('home/locked/true', this.lock);
        this.props.homeWS.onRequest('home/locked/false', this.unlock);
    };

    protected clear(): void { };

    protected getClassName = (): string => {
        const defaultClassAttr = 'home-locker-component';

        if (Helper.isUndefined(this.props.className)) {
            return defaultClassAttr + ' ' + (this.state.isHomeLocked ? 'locked' : '');
        }

        return defaultClassAttr + ' ' + this.props.className + ' ' + (this.state.isHomeLocked ? 'locked' : '');
    }

    protected turnHomeLockedStatus = (): void => {
        this.props.homeWS.sendRequest(`home/home/locked/` + (this.state.isHomeLocked ? 'false' : 'true'));
    };

    protected lock = (): void => {
        this.setState((prevState: IState) => ({
            isHomeLocked: true,
        }));
    }

    protected unlock = (): void => {
        this.setState((prevState: IState) => ({
            isHomeLocked: false,
        }));
    }


    render(): React.ReactNode {
        return (
            <div id={this.setComponentId()} className={this.getClassName()} onClick={() => this.turnHomeLockedStatus()}>
                <div>
                    <IconHandler icon={this.state.isHomeLocked ? 'lock' : 'lock-open'} />
                </div>

                <p>{this.state.isHomeLocked ? 'Unlock' : 'Lock'} your home</p>
            </div>
        );
    }
}
