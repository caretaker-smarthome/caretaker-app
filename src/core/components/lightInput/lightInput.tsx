import * as React from 'react';
import IconHandler from '../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './lightInput.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import LightIcon from './assets/light.svg';
// END IMPORT IMAGE ZONE


// IMPORT INTERFACE ZONE
import {
    TLanguages,
} from '../../../commonInterface';
import WebsocketHandler from '../../../core/api/websocketHandler';
import Helper from '../../../helper';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    componentName: string;
    homeWS: WebsocketHandler;
    className?: string,
}

interface IState {
    isLightOn: boolean;
}

// END INTERFACES ZONE


export default class LightInput extends ComponentBase<IProps, IState> {

    protected componentId = 'light';

    constructor(props: IProps) {
        super(props);

        this.state = {
            isLightOn: false,
        };
    }

    protected init = (): void => {
        this.props.homeWS.onRequest(this.props.componentName + '/turn/on', this.turnLightOn);
        this.props.homeWS.onRequest(this.props.componentName + '/turn/off', this.turnLightOff);
    };

    protected clear(): void { };

    protected getClassName = (): string => {
        const defaultClassAttr = 'lightInput';

        if (Helper.isUndefined(this.props.className)) {
            return defaultClassAttr + ' ' + (this.state.isLightOn ? 'active' : 'test');
        }

        return defaultClassAttr + ' ' + this.props.className + ' ' + (this.state.isLightOn ? 'active' : '');
    }

    protected turnLightOn = (): void => {
        this.setState((prevState: IState) => ({
            isLightOn: true,
        }));
    }

    protected turnLightOff = (): void => {
        this.setState((prevState: IState) => ({
            isLightOn: false,
        }));
    }

    protected turnLight = (): void => {
        this.props.homeWS.sendRequest(`home/${this.props.componentName}/turn/` + (this.state.isLightOn ? 'off' : 'on'));
    };

    render(): React.ReactNode {
        return (
            <div id={this.setComponentId()} className={this.getClassName()} onClick={() => { this.turnLight() }}>
                <LightIcon />

                <p>{this.props.componentName}</p>
            </div>
        );
    }
}
