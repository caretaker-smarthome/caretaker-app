import * as React from 'react';
import IconHandler from '../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './fireplaceInput.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../assets/uiComponents/base/componentBase';
import SwitchCheckbox from '../../../assets/uiComponents/switchCheckbox/switchCheckbox';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import FireplaceIcon from './assets/fireplace.svg';
// END IMPORT IMAGE ZONE


// IMPORT INTERFACE ZONE
import {
    TLanguages,
} from '../../../commonInterface';
import WebsocketHandler from '~core/api/websocketHandler';
import Helper from '~helper';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    componentName: string;
    homeWS: WebsocketHandler;
    className?: string,
}

interface IState {
    isFireplaceOn: boolean;
}

// END INTERFACES ZONE


export default class FireplaceInput extends ComponentBase<IProps, IState> {

    protected componentId = 'fireplace';

    constructor(props: IProps) {
        super(props);

        this.state = {
            isFireplaceOn: false,
        };
    }

    protected init = (): void => {
        this.props.homeWS.onRequest(this.props.componentName + '/turn/on', this.turnFireplaceOn);
        this.props.homeWS.onRequest(this.props.componentName + '/turn/off', this.turnFireplaceOff);
    };

    protected clear(): void { };

    protected getClassName = (): string => {
        const defaultClassAttr = 'fireplaceInput';

        if (Helper.isUndefined(this.props.className)) {
            return defaultClassAttr + ' ' + (this.state.isFireplaceOn ? 'active' : '');
        }

        return defaultClassAttr + ' ' + this.props.className + ' ' + (this.state.isFireplaceOn ? 'active' : '');
    }

    protected turnFireplaceOn = (): void => {
        this.setState((prevState: IState) => ({
            isFireplaceOn: true,
        }));
    }

    protected turnFireplaceOff = (): void => {
        this.setState((prevState: IState) => ({
            isFireplaceOn: false,
        }));
    }

    protected turnFireplace = (): void => {
        this.props.homeWS.sendRequest(`home/${this.props.componentName}/turn/` + (this.state.isFireplaceOn ? 'off' : 'on'));
    };

    render(): React.ReactNode {
        return (
            <div id={this.setComponentId()} className={this.getClassName()} onClick={() => { this.turnFireplace() }}>
                <FireplaceIcon />

                <p>{this.props.componentName}</p>
            </div>
        );
    }
}
