import * as React from 'react';
import IconHandler from '../../../assets/uiComponents/iconHandler/iconHandler';

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT STYLES ZONE
import './temperatureDisplay.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../assets/uiComponents/base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import ThermometerIcon from './assets/thermometer.svg';
// END IMPORT IMAGE ZONE


// IMPORT INTERFACE ZONE
import {
    TLanguages,
} from '../../../commonInterface';
import WebsocketHandler from '../../../core/api/websocketHandler';
import Helper from '../../../helper';
// END IMPORT INTERFACE ZONE


// INTERFACES ZONE
interface IProps {
    componentName: string;
    homeWS: WebsocketHandler;
    className?: string,
}

interface IState {
    temperatureValue: number;
}

// END INTERFACES ZONE


export default class temperatureDisplay extends ComponentBase<IProps, IState> {

    protected componentId = 'temperature';

    constructor(props: IProps) {
        super(props);

        this.state = {
            temperatureValue: 0
        };
    }

    protected init = (): void => {
        this.props.homeWS.onRequest(`${this.props.componentName}/temperature/:temperature`, this.onNewTemperature)
    };

    protected clear(): void { };

    private onNewTemperature = (variables: any): void => {
        this.setState((prevState: IState) => ({
            temperatureValue: parseFloat(variables['temperature']),
        }));
    }

    protected getClassName = (): string => {
        const defaultClassAttr = 'temperatureDisplay';

        if (Helper.isUndefined(this.props.className)) {
            return defaultClassAttr;
        }

        return defaultClassAttr + ' ' + this.props.className;
    }

    render(): React.ReactNode {
        return (
            <div id={this.setComponentId()} className={this.getClassName()}>
                <ThermometerIcon />
                <p className="value">{this.state.temperatureValue}°</p>
                <p>{this.props.componentName}</p>
            </div>
        );
    }
}
