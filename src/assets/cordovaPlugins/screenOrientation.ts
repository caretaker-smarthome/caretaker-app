import CordovaPluginBase from './cordovaPluginBase';

// IMPORT INTERFACE ZONE
import { TScreenOrientation } from '../../commonInterface';
// END IMPORT INTERFACE ZONE

export default class ScreenOrientationPlugin extends CordovaPluginBase {
    constructor() {
        super();
    }

    protected init = (): void => {
        this.lockScreenTo('portrait');
    }

    protected lockScreenTo(screenOrientation: TScreenOrientation): void {
        window.screen.orientation.lock(screenOrientation)
    }
}