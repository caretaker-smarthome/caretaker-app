import CordovaPluginBase from './cordovaPluginBase';


export default class ConnexionStatusPlugin extends CordovaPluginBase {

    protected onOnline: () => void;
    protected onOffline: () => void;

    constructor(onOnline: () => void, onOffline: () => void) {
        super();

        this.onOnline = onOnline;
        this.onOffline = onOffline;
    }

    protected init = (): void => {
        document.addEventListener("online", this.onOnline, false);
        document.addEventListener("offline", this.onOffline, false);
    }
}