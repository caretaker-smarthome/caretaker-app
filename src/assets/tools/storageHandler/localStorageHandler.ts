import Helper from '../../../helper';

export default class LocalStorageHandler {

    protected static LOCALSTORAGE_PREFIX = "CS_";

    public static getItem(itemName: string): string {
        return localStorage.getItem(this.LOCALSTORAGE_PREFIX + itemName);
    }

    // TODO: Refactoring
    public static setItem(itemName: string, itemValue: string | Object): void {
        if (itemValue instanceof Object) {
            localStorage.setItem(this.LOCALSTORAGE_PREFIX + itemName, Helper.objectToString(itemValue));
        } else {
            localStorage.setItem(this.LOCALSTORAGE_PREFIX + itemName, itemValue);
        }
    }

    public static deleteItem(itemName: string): void {
        localStorage.removeItem(this.LOCALSTORAGE_PREFIX + itemName);
    }

}