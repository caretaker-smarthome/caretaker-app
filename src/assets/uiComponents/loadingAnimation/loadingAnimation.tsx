import * as React from 'react';


// IMPORT STYLES ZONE
import './loadingAnimation.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT ASSETS ZONE
// @ts-ignore
import SimpleIcon from '../../img/simpleIcon.svg';
// END IMPORT ASSETS ZONE

interface IProps { }

interface IState { }


export default class LoadingAnimation extends ComponentBase<IProps, IState> {

    protected componentId = 'loadingAnimation';

    constructor(props: IProps) {
        super(props);

        this.state = {};

    }


    render() {
        return (
            <div id={this.setComponentId()}>

                <div id={this.setIdTo("animationCtn")}>
                    <div className="loader">
                        <SimpleIcon />
                    </div>
                </div>

            </div >
        );
    }
}
