import * as React from 'react';
import IconHandler from '../iconHandler/iconHandler';


// IMPORT STYLES ZONE
import './serverErrorModal.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT INTERFACE ZONE
// END IMPORT INTERFACE ZONE

// INTERFACE ZONE
interface IProps { }
// END INTERFACE ZONE

export default class ServerErrorModal extends ComponentBase<IProps> {

    protected componentId = 'serverErrorModal';


    render() {
        return (
            <div
                id={this.setComponentId()}
                className="modal fade"
                role="dialog"
                aria-labelledby="alertModalLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div className="alert alert-danger" role="alert">
                        <IconHandler icon="times-circle" className="with-pr" />
                        Impossible to communicate with the server
                    </div>
                </div>
            </div>
        );
    }
}
