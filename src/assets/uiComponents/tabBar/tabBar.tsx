import * as React from 'react';

// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../base/componentBase';
import IconHandler from '../iconHandler/iconHandler';
// END IMPORT COMPONENTS ZONE

// IMPORT STYLES ZONE
import './tabBar.scss';
// END IMPORT STYLES ZONE

// IMPORT LOCALIZE ZONE
// END IMPORT LOCALIZE ZONE

// IMPORT IMAGE ZONE
// END IMPORT IMAGE ZONE

// IMPORT INTERFACE ZONE
import {
    TPagesInMain,
    ISimpleModalParams,
} from '../../../commonInterface';
// END IMPORT INTERFACE ZONE

interface IProps {
    currentPage: TPagesInMain,
    goToPage: (pageName: TPagesInMain) => void,
    showSimpleModal: (params: ISimpleModalParams) => void,
    isConnectedToHome: boolean,
}

interface IState { }

export default class TabBar extends ComponentBase<IProps, IState> {

    protected componentId = 'tabBarCtn';

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    protected init = (): void => { }

    protected goToQuickActionPage(): void {
        if (this.props.isConnectedToHome) {
            this.props.goToPage('quickActionPage')
        } else {
            this.props.showSimpleModal({
                type: 'danger',
                message: 'You have to be connected to a house !',
            })

            this.props.goToPage('myHomePage');
        }
    }


    render() {
        return (
            <div id={this.setComponentId()}>
                <div
                    className={this.props.currentPage === 'myHomePage' ? "clickable selected" : "clickable"}
                    onClick={() => { this.props.goToPage('myHomePage') }}
                >
                    <IconHandler icon="home" />
                </div>
                <div
                    className={this.props.currentPage === 'quickActionPage' ? "clickable selected" : "clickable"}
                    onClick={() => { this.goToQuickActionPage() }}
                >
                    <IconHandler icon="bolt" />
                </div>

                {/* TODO: What do we do with this page ? */}
                <div
                    className={this.props.currentPage === 'quickActionPage' ? "clickable" : "clickable"}
                    onClick={() => { }}
                >
                    <IconHandler icon="bell" />
                </div>
            </div >
        );
    }
}
