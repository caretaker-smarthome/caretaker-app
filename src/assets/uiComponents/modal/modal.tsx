import * as React from 'react';
import ComponentBase from '../base/componentBase';

// IMPORT STYLES ZONE
import './modal.scss';
// END IMPORT STYLES ZONE

// IMPORT INTERFACE ZONE
import { IModalParams } from '../../../commonInterface';
// END IMPORT INTERFACE ZONE

// INTERFACE ZONE
interface IProps {
    params: IModalParams,
}
// END INTERFACE ZONE

export default class Modal extends ComponentBase<IProps> {

    protected componentId = 'modal';
    protected modalComponent: JQuery;

    constructor(props: IProps) {
        super(props);
    }

    protected init(): void {
        this.modalComponent = $('#' + this.getComponentId());
    }

    protected onOk = (): void => {
        (this.props.params.onOk)();
        this.modalComponent.modal('hide');
    }


    render() {
        return (
            <div
                id={this.setComponentId()}
                className="modal fade"
                tabIndex={-1}
                role="dialog"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">{this.props.params.title}</h5>
                            <button
                                type="button"
                                className="close"
                                data-dismiss="modal"
                                aria-label="Close"
                            >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div className="modal-body">
                            {this.props.params.body}
                        </div>

                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary" onClick={() => { this.onOk() }}>Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
