import * as React from 'react';

// IMPORT HELPER ZONE
import Helper from '../../../helper';
// END IMPORT HELPER ZONE

// IMPORT STYLES ZONE
import './connexionStatus.scss';
// END IMPORT STYLES ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT LOCALIZE ZONE
import LOCALIZE from './localize'
// END IMPORT LOCALIZE ZONE

// IMPORT INTERFACE ZONE
import { TConnexionStatus, TLanguages } from '../../../commonInterface';
// END IMPORT INTERFACE ZONE

// IMPORT COMPONENTS ZONE
// END IMPORT COMPONENTS ZONE

// INTERFACES ZONE
interface IProps {
    language: TLanguages,
    status: TConnexionStatus,
}
// END INTERFACES ZONE


export default class ConnexionStatus extends ComponentBase<IProps> {

    protected componentId = 'connexionStatus';
    protected connexionStatusOnline: JQuery;

    constructor(props: IProps) {
        super(props);
    }

    protected init = (): void => {
        this.connexionStatusOnline = $('#' + this.getComponentId() + '.online');
    }

    protected getConnexionStatusComponentClassName(): string {
        return this.props.status === 'online' ? 'online' : 'offline';
    }

    componentDidUpdate(prevProps: IProps) {
        if (prevProps.status !== this.props.status) {
            if (this.props.status === 'online') {
                this.connexionStatusOnline.css({ display: 'inline', });
            } else {
            }
        }
    }


    render() {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} className={this.getConnexionStatusComponentClassName()}>
                {localize[this.props.status]}
            </div>
        );
    }
}
