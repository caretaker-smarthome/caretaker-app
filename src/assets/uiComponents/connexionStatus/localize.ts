const LOCALIZE = {
    'de': {
        online: 'Verbindung wiederhergestellt !',
        offline: 'keine Verbindung...'

    },
    'fr': {
        online: 'connexion rétablie !',
        offline: 'aucune connexion...'
    },

    'en': {
        online: 'connection restored !',
        offline: 'no connection...'
    },
};

export default LOCALIZE;