import * as React from 'react';

import ComponentBase from '../componentBase';
import IconHandler from '../../iconHandler/iconHandler';

// IMPORT STYLES ZONE
import './popupBase.scss';
// END IMPORT STYLES ZONE

export default abstract class PopupBase<IProps, IState> extends ComponentBase<IProps, IState>  {

    constructor(props: IProps) {
        super(props);
    }

    protected ANIMATION_DURATION = 500;

    protected abstract popupName: string;
    protected abstract init(): void;
    protected abstract clear(): void;

    protected abstract popupBodyRender(): React.ReactNode;

    protected openPopup(): void {
        const popupComponent = $('#' + this.componentId);
        popupComponent.css({
            animation: 'popupApparition ' + this.ANIMATION_DURATION + 'ms',
        });
    }

    protected closePopup(): void {
        const popupComponent = $('#' + this.componentId + '.popup');
        popupComponent.css({
            animation: 'popupDisparition ' + this.ANIMATION_DURATION + 'ms forwards',
        });

        this.clear();
    }

    protected popupHeaderRender(): React.ReactNode {
        return (
            <div className="popup-header">
                <IconHandler icon="arrow-left" className="go-back-icon clickable" onClick={() => this.closePopup()} />
                <h1>{this.popupName}</h1>
            </div>
        )
    }


    render(): React.ReactNode {
        return (
            <div id={this.componentId} className="popup">
                {this.popupHeaderRender()}
                {this.popupBodyRender()}
            </div>
        )
    }
}