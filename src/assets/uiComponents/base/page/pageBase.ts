import ComponentBase from '../componentBase';

// IMPORT STYLES ZONE
import './pageBase.scss';
// END IMPORT STYLES ZONE

import { TPopups } from '../../../../commonInterface';

export default abstract class PageBase<IProps, IState> extends ComponentBase<IProps, IState>  {

    constructor(props: IProps) {
        super(props);
    }

    protected openPopup(popupId: TPopups): void {
        const popupComponent = $('#' + popupId + '.popup');

        popupComponent.css({
            display: 'inline',
            animation: 'popupApparition 500ms',
        });
    }

    protected closePopup(popupId: TPopups): void {
        const popupComponent = $('#' + popupId + '.popup');

        popupComponent.css({
            animation: 'popupDisparition 500ms forwards',
        });
    }

    protected abstract init(): void;
    protected clear(): void { };
}