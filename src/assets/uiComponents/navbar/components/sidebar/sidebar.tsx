import * as React from 'react';

// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../../../base/componentBase';
import SimpleNavItem from './components/simpleNavItem/simpleNavItem';
// END IMPORT COMPONENTS ZONE

// IMPORT STYLES ZONE
import './sidebar.scss';
// END IMPORT STYLES ZONE

// IMPORT APICMDS ZONE
import ApiCmds from '../../../../../core/api/apiCmds';
// END IMPORT APICMDS ZONE

// IMPORT LOCALIZE ZONE
import COMMON_LOCALIZE from '../../../../../commonLocalize';
import LOCALIZE from './localize';
// END IMPORT LOCALIZE ZONE

// IMPORT IMAGE ZONE
// @ts-ignore
import FrFlagSvg from '../../../../img/fr.svg';
// @ts-ignore
import DeFlagSvg from '../../../../img/de.svg';
// @ts-ignore
import UsFlagSvg from '../../../../img/us.svg';
// END IMPORT IMAGE ZONE

// IMPORT INTERFACE ZONE
import {
    TLanguages,
    ISetLangResponse,
    TPagesInMain,
    TFontawesomeIconClass
} from '../../../../../commonInterface';
// END IMPORT INTERFACE ZONE

// INTERFACE ZONE
interface IFlagsClass {
    en: string,
    de: string,
    fr: string,
}
// END INTERFACE ZONE


interface IProps {
    goToPage: (pageName: TPagesInMain) => void,
    setLanguage: (language: TLanguages) => void,
    language: TLanguages,
    userPseudo: string,
    userAvatarLink: string,
    logoutUser: () => void,
    showOrHideSidebar: () => void,
}

interface IState {
    showSidebar: boolean,
}

export default class Sidebar extends ComponentBase<IProps, IState> {

    protected componentId = 'sidebar';
    protected sidebar: JQuery;
    protected sidebarNav: JQuery;

    constructor(props: IProps) {
        super(props);

        this.state = {
            showSidebar: false,
        };
    }

    protected init = (): void => {
        this.initComponents();
    }

    protected initComponents = (): void => {
        this.sidebar = $(this.getId(this.getComponentId()));
        this.sidebarNav = $(this.getId('sidebarNav'));
    }

    protected setLanguage = (language: TLanguages): void => {
        this.props.setLanguage(language);

        const normalizeLangRequest = {
            lang: language,
        }

        ApiCmds.setLang(normalizeLangRequest,
            (result: ISetLangResponse) => {
                this.onSetLangCallback(result);
            });

        this.props.showOrHideSidebar();
    }

    protected onSetLangCallback(result: ISetLangResponse): void { }

    protected goToPage = (pageName: TPagesInMain): void => {
        this.props.goToPage(pageName);
        this.props.showOrHideSidebar();
    }

    protected getUserLangFlag = (): any => {
        switch (this.props.language) {
            case 'fr':
                return <FrFlagSvg />

            case 'de':
                return <DeFlagSvg />

            case 'en':
                return <UsFlagSvg />
        }
    }

    protected flagsDropdownContentRender = () => {
        const selectedLang = this.props.language;
        const commonLocalize = COMMON_LOCALIZE[selectedLang];


        // TODO: Refactoring flagsDropdownContentRender method
        return (
            <div className="dropdown-menu" aria-labelledby={this.setIdTo("langFlagDropdown")}>
                {selectedLang === 'fr' ? '' :
                    <a className="dropdown-item" onClick={() => { this.setLanguage('fr') }}>
                        <FrFlagSvg />
                        {commonLocalize['french']}
                    </a>
                }
                {selectedLang === 'de' ? '' :
                    <a className="dropdown-item" onClick={() => { this.setLanguage('de') }}>
                        <DeFlagSvg />
                        {commonLocalize['german']}
                    </a>
                }
                {selectedLang === 'en' ? '' :
                    <a className="dropdown-item" onClick={() => { this.setLanguage('en') }}>
                        <UsFlagSvg />
                        {commonLocalize['english']}
                    </a>
                }
            </div>
        )
    }

    protected pseudoHeaderRender() {
        return (
            <div id={this.setIdTo("pseudoHeader")} onClick={() => { /*this.goToPage('userProfilPage')*/ }}>
                <img id={this.setIdTo("avatar")} src={this.props.userAvatarLink} />
                <h2 className="pseudo">
                    {this.props.userPseudo}
                </h2>
            </div>
        )
    }

    protected simpleNavItemRender(navItemText: string, navItemIcon: TFontawesomeIconClass, onClick?: () => void) {
        return (
            <SimpleNavItem
                text={navItemText}
                icon={navItemIcon}
                onClick={onClick}
            />
        )
    }


    render() {
        const localize = LOCALIZE[this.props.language];

        return (
            <div id={this.setComponentId()} >
                <nav id={this.setIdTo("sidebarNav")} className="with-scrollbar">

                    {this.pseudoHeaderRender()}

                    <ul className="navbar-nav">
                        {this.simpleNavItemRender(localize.sponsors, 'handshake', () => { this.goToPage('sponsorsPage') })}

                        <li className="nav-item dropdown">
                            <a
                                id={this.setIdTo("langFlagDropdown")}
                                className="nav-link dropdown-toggle"
                                role="button"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                {this.getUserLangFlag()}
                            </a>
                            {this.flagsDropdownContentRender()}
                        </li>

                        {this.simpleNavItemRender(localize.contact, 'phone', () => { this.goToPage('contactPage') })}
                        {this.simpleNavItemRender(localize.settings, 'cog', () => { this.goToPage('settingsPage') })}
                        {this.simpleNavItemRender(localize.logout, 'power-off', () => { this.props.logoutUser() })}
                    </ul>
                </nav>
            </div>
        );
    }
}
