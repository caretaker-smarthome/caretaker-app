const LOCALIZE = {
    "de": {
        my_home: "Mein Haus",
        logout: "Trennung",
        calendar: "Kalendar",
        contact: "Contact",
        myGifts: "Meine Geschenke",
        sponsors: "Sponsors",
        quickActions: "Quick Actions",
        settings: "Einstellungen",
    },
    "fr": {
        my_home: "Ma maison",
        logout: "Déconnexion",
        calendar: "Calendrier",
        contact: "Contact",
        myGifts: "Mes cadeaux",
        sponsors: "Sponsors",
        quickActions: "Actions rapides",
        settings: "Paramètres",
    },
    "en": {
        my_home: "My home",
        logout: "Logout",
        calendar: "Calendar",
        contact: "Contact",
        myGifts: "My gifts",
        sponsors: "Sponsors",
        quickActions: "Quick Actions",
        settings: "Settings",
    },
};

export default LOCALIZE;