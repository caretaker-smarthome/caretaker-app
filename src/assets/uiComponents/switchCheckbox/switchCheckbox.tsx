import * as React from 'react';

// IMPORT HELPER ZONE
import Helper from '../../../helper';
// END IMPORT HELPER ZONE

// IMPORT COMPONENTS ZONE
import ComponentBase from '../base/componentBase';
// END IMPORT COMPONENTS ZONE

// IMPORT STYLES ZONE
import './switchCheckbox.scss';
// END IMPORT STYLES ZONE

// IMPORT INTERFACE ZONE
// END IMPORT INTERFACE ZONE

// INTERFACE ZONE
interface IProps {
    checked: boolean,
    onChange: () => void
    className?: string,
}
// END INTERFACE ZONE

export default class SwitchCheckbox extends ComponentBase<IProps> {

    protected componentId = 'switchCheckbox';

    protected getClassName = (): string => {
        const defaultClassAttr = 'switch-checkbox';

        if (Helper.isUndefined(this.props.className)) {
            return defaultClassAttr;
        }

        return defaultClassAttr + ' ' + this.props.className;
    }

    render() {
        return (
            <label className={this.getClassName()}>
                <input
                    type="checkbox"
                    checked={this.props.checked}
                    onChange={() => { this.props.onChange() }}
                />
                <span className="slider round"></span>
            </label>
        );
    }
}
