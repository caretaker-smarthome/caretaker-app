# Caretaker-app


----
### npm CLI:
* `npm run dev` : build project in `/build/dev` folder and open it on server `http://localhost:1234` (**Warning**: all links are absolute)
* `npm run devLocal` : build project with relative links in `/build/dev` folder
* `npm run prod` : build project with relative links and minify code in `/build/prod` folder


----
### This project use:
* Web app bundler :
    * [ParcelJs](https://en.parceljs.org/)

* HTML/CSS :
    * [Bootstrap](https://getbootstrap.com/docs/4.2/getting-started/introduction/) for styles
    * [font-awesome](https://fontawesome.com/icons?d=gallery) for icons
    * [Sass](https://sass-lang.com/documentation/file.SASS_REFERENCE.html) to write css styles more quickly
        
* JS :
    * [Jquery](https://api.jquery.com/) to use Bootstrap
    * [ReactJS](https://reactjs.org/docs/getting-started.html) to easily create an user interface


----
### Installation:

1. Edit `app-settings.json` in `root` directory with yours settings
    ```json
    {
        "name": "Connected_house-app",
        "version": "<version>",
        "use_cordova": <boolean>
        "path_to_imgs": "<link/to/imgs>",
        "api_to_use": "<local || server>",
        "ws_to_use": "<local || server>",
        "api": {
            "local": "<link/to/api>",
            "server": "<link/to/api>"
        },
        "ws": {
            "local": "<path/to/websocket/commands>",
            "server": "ws://api.caretaker-smarthome.eu/src/api/ws/"
        }
    }
    ```