const fs = require('fs');
const readline = require('readline');
const SETTINGS = require('../app-settings.json');
const ABSOLUTE_PATH_TO_PROJECT = 'C:\\UwAmp\\www\\isfates_adventskalender';
const apiToUse = SETTINGS['api_to_use'];

const deleteFolderRecursive = function (path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

const asciiScriptTitle = `
__       __  ___        __                           __
\\ \\     /  |/  /__  ___/ /__       ___  _______  ___/ /
 > >   / /|_/ / _ \\/ _  / -_)     / _ \\/ __/ _ \\/ _  / 
/_/   /_/  /_/\\___/\\_,_/\\__/     / .__/_/  \\___/\\_,_/  
                                /_/                    
`;



console.log(asciiScriptTitle + '\n');


deleteFolderRecursive(ABSOLUTE_PATH_TO_PROJECT + '/build/prod');

if (apiToUse !== 'server') {
    console.log('"api_to_use" parameter is not set to "server" in your "app-settings.json"\n');
    console.log(`> "api_to_use" : "${apiToUse}"`);
    console.log(`                  ${('^').repeat(apiToUse.length)}\n\n`);

    rl.question(">>> Do you want to continue anyway? Yes/No: ", (answer) => {

        if (answer !== "Yes") {
            process.exit(1);
        } else {
            process.exit(0);
        }

        rl.close();
    });
} else {
    process.exit(0);
}
